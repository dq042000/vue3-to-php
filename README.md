# PhpSpreadsheet

## 啟動
```shell=
docker-compose up -d --build
```

## 安裝
```shell=
docker exec -ti vue3-php_php_1 composer install

docker exec -ti vue3-php_vite_1 yarn

docker exec -ti vue3-php_vite_1 yarn dev
```